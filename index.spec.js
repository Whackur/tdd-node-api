const app = require('./index');
const request = require('supertest');
const should = require('should');

describe('GET /users 는 ', () => {
  describe('성공 시', () => {
    it('유저 객체를 담은 배열로 응답한다. ', (done) => {
      request(app)
        .get('/users')
        .end((err, res) => {
          res.body.should.be.instanceOf(Array); //배열인지 확인
          done(); // done이라는 콜백함수를 넘겨줌
        })
    })
    it('유저 객체를 담은 배열로 응답한다. ', (done) => {
      request(app)
        .get('/users?limit=2')
        .end((err, res) => {
          res.body.should.have.lengthOf(2); // length 2여야 함
          done(); // done이라는 콜백함수를 넘겨줌
        })
    })
  })
  describe('실패 시', () => {
    it('limit이 숫자형이 아니면 400을 응답한다.', done => {
      request(app)
        .get('/users?limit=two')
        .expect(400)
        .end(done)
    })
  })
});

describe('GET /users/1 는 ', () => {
  describe('성공 시 ', () => {
    it('id 가 1인 유저 객체를 반환한다.', done => {
      request(app)
        .get('/users/1')
        .end((err, res) => {
          res.body.should.have.property('id', 1);
          done();
        })
    })
  })
  describe('실패 시', () => {
    it('id가 숫자가 아닐 경우 400으로 응답한다.', done => {
      request(app)
        .get('/users/one')
        .expect(400)
        .end(done)
    })
    it('id로 유저를 찾을 수 없는 경우 404으로 응답한다.', done => {
      request(app)
        .delete('/users/999')
        .expect(404)
        .end(done)
    })
  })
});

describe('DELETE /users/1 는', () => {
  describe('성공 시', () => {
    it('204를 응답한다', (done) => {
      request(app)
        .delete('/users/1')
        .expect(204)
        .end(done);
    })
  })
  describe('실패 시', () => {
    it('id가 숫자가 아닐경우 400으로 응답', done => {
      request(app)
        .delete('/users/one')
        .expect(400)
        .end(done)
    })
  })
})

describe('POST /users', () => {
  describe('성공 시 ', () => {
    let name = 'daniel'
    let body;
    // before 은 테스트케이스 시작 전 실행
    before(done => {
      request(app)
        .post('/users')
        .send({name})
        .expect(201)
        .end((err, res) => {
          body = res.body;
          done();
        })
    })
    it('생성된 유저 객체를 반환한다.', () => {
      body.should.have.property('id')
    })
    it('입력한 name을 반환한다.', () => {
      body.should.have.property('name')
    })
  })
})
// ~ 42